﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Salazar.Framework;
using Prime31.MessageKit;
using UnityEngine.EventSystems;

namespace Salazar.UI
{

    public class InventoryItemUI : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] Image image;

        InventoryItem item;
    	
        public InventoryItemUI Init (InventoryItem item)
        {
            this.item = item;
            image.sprite = item.image;
            this.gameObject.SetActive (true);
            return this;
        }

        #region IPointerClickHandler implementation
        public void OnPointerClick (PointerEventData eventData)
        {
            
        }
        #endregion

        void ExamineItem ()
        {
        }
    }
}
