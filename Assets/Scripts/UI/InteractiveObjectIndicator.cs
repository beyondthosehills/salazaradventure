﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.MessageKit;
using Salazar.Framework;

namespace Salazar.UI
{

    public class InteractiveObjectIndicator : MonoBehaviour 
    {
        void Awake ()
        {
            MessageKit<bool>.addObserver (GameMessage.CheckInteractiveObject, OnCheckInteractiveObject);
        }

        void OnDestroy ()
        {
            MessageKit<bool>.removeObserver (GameMessage.CheckInteractiveObject, OnCheckInteractiveObject);
        }

        void OnCheckInteractiveObject (bool value)
        {
            this.gameObject.SetActive (value);
        }
    }
}
