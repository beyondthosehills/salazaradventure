﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Salazar.Framework;
using Prime31.MessageKit;

namespace Salazar.UI
{

    public class InventoryUI : MonoBehaviour 
    {
        [SerializeField] Transform inventoryParent;
        [SerializeField] InventoryItemUI itemTemplate;

        Dictionary<InventoryItem,InventoryItemUI> itemUIDictionary = new Dictionary<InventoryItem, InventoryItemUI>();

    	void Awake () 
        {
            MessageKit<InventoryItem>.addObserver (GameMessage.AddInventoryItem, OnAddItem);
            MessageKit<InventoryItem>.addObserver (GameMessage.RemoveInventoryItem, OnRemoveItem);
    	}

        void OnDestroy ()
        {
            MessageKit<InventoryItem>.removeObserver (GameMessage.AddInventoryItem, OnAddItem);
            MessageKit<InventoryItem>.removeObserver (GameMessage.RemoveInventoryItem, OnRemoveItem);
        }

        void OnAddItem (InventoryItem item)
        {
            if (itemUIDictionary.ContainsKey (item))
                return;
            
            itemUIDictionary.Add (item,Instantiate<InventoryItemUI> (itemTemplate, inventoryParent).Init (item));
        }

        void OnRemoveItem (InventoryItem item)
        {
            if (!itemUIDictionary.ContainsKey (item))
                return;

            Destroy (itemUIDictionary[item]);

            itemUIDictionary.Remove (item);
        }
    }
}
