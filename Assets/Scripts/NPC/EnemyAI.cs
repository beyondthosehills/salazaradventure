﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Salazar.NPC
{

    public class EnemyAI : MonoBehaviour 
    {
        [SerializeField] Transform initLocation;
        [SerializeField] float range;
        [SerializeField] float yTolerance;

        public NavMeshAgent agent { get; private set; } 


        Transform playerTransform;
        float playerDistance;
        float playerYDistance;

    	// Use this for initialization
    	void Start () 
        {
            playerTransform = GameObject.FindGameObjectWithTag ("Player").transform; 
            this.transform.position = initLocation.position;

            agent = GetComponentInChildren<NavMeshAgent>();
            agent.updateRotation = false;
            agent.updatePosition = true;
    	}


    	// Update is called once per frame
    	void Update () 
        {
            if (playerTransform == null)
                return;

            playerDistance = Vector3.Distance (playerTransform.position, initLocation.position);
            playerYDistance = Mathf.Abs (initLocation.position.y - playerTransform.position.y);

            if (playerDistance < range && playerYDistance < yTolerance)
            {
                agent.SetDestination (playerTransform.position);
            }
            else
            {
                agent.SetDestination (initLocation.position);
            }
    	}

        void OnDrawGizmos ()
        {
            if (initLocation == null)
                return;

            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere (initLocation.position, range);
        }
    }
}
