﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Salazar.NPC
{
    public class EnemyAttack : MonoBehaviour 
    {

        void OnTriggerEnter (Collider other)
        {
            if(other.tag == "Player")
            {
                SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex, LoadSceneMode.Single);
            }
        }
    	
    }
}