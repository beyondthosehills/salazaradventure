﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Salazar.NPC
{

    public class LookAtPlayer : MonoBehaviour 
    {
        Transform playerTransform;

    	// Use this for initialization
    	void Start () 
        {
            playerTransform = GameObject.FindGameObjectWithTag ("Player").transform;	
    	}
    	
    	// Update is called once per frame
    	void Update () 
        {
            this.transform.rotation = playerTransform.rotation;
    	}
    }
}
