﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Salazar.NPC
{
    public class EnemyLife : MonoBehaviour 
    {
        [SerializeField] float initLife;

        float currentLife;

    	// Use this for initialization
    	void Start () 
        {
            currentLife = initLife;
    	}
    	
        public void OnDamage (float damageSpeed)
        {
            currentLife = currentLife - (damageSpeed * Time.deltaTime);

            if (currentLife < 0)
            {
                Destroy (this.gameObject);
            }
        }
    }
}
