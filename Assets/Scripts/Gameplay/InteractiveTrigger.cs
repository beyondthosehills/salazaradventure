﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Salazar.Framework;

namespace Salazar.Gameplay
{
    [AddComponentMenu("Salazar/Interactive Trigger")]
    [RequireComponent(typeof(Collider))]
    public class InteractiveTrigger : MonoBehaviour 
    {
        [SerializeField] State[] states;
        [SerializeField] InventoryItem[] items;

        void OnTriggerEnter (Collider other)
        {
            if (other.tag == "Player")
            {
                Interact ();
            }
        }

        void Interact ()
        {
            Debug.LogFormat ("Interact with object {0}", name);

            if (!CheckStates ())
                return;

            if (!CheckInventory ())
                return;

            GenericBehaviour[] actions = GetComponents<GenericBehaviour> ();

            if (actions == null)
                return;

            foreach (GenericBehaviour action in actions)
            {
                if (action != null)
                    action.Behave ();
            }
        }

        bool CheckStates ()
        {
            if (states == null || states.Length == 0)
                return true;

            foreach (State state in states)
            {
                if (!StateManager.HasState (state))
                    return false;
            }

            return true;
        }

        bool CheckInventory ()
        {
            if (items == null || items.Length == 0)
                return true;

            foreach (InventoryItem item in items)
            {
                if (!InventoryManager.HasItem (item))
                    return false;
            }

            return true;
        }
    }
}