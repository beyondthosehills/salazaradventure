﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Salazar.Framework;
using Prime31.MessageKit;

namespace Salazar.Gameplay
{
    [AddComponentMenu("Salazar/Actions/Unset State")]
    public class UnsetStateBehaviour : GenericBehaviour 
    {
        [SerializeField] State state;

        public override void Behave ()
        {
            base.Behave ();

            MessageKit<State>.post (GameMessage.UnsetState, state);
        }
    }
}
