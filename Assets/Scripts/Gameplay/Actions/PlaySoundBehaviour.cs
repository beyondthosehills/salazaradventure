﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Salazar.Framework;
using Prime31.MessageKit;

namespace Salazar.Gameplay
{
    [AddComponentMenu("Salazar/Actions/Play Sound")]
    public class PlaySoundBehaviour : GenericBehaviour 
    {
        [SerializeField] AudioSource AudioSource;

        public override void Behave ()
        {
            base.Behave ();

            AudioSource.Play ();
        }
    }
}
