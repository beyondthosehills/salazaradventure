﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Salazar.Gameplay
{
    [AddComponentMenu("Salazar/Actions/Set Active Value")]
    public class SetActiveBehaviour : GenericBehaviour 
    {
        [SerializeField] GameObject objectToSetActive;
        [SerializeField] bool activeValue;

        public override void Behave ()
        {
            base.Behave ();

            objectToSetActive.SetActive (activeValue);
        }
    }
}
