﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Salazar.Framework;
using Prime31.MessageKit;

namespace Salazar.Gameplay
{
    [AddComponentMenu("Salazar/Actions/Set State")]
    public class SetStateBehaviour : GenericBehaviour 
    {
        [SerializeField] State state;

        public override void Behave ()
        {
            base.Behave ();

            MessageKit<State>.post (GameMessage.SetState, state);
        }
    }
}
