﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Salazar.Framework;
using Prime31.MessageKit;

namespace Salazar.Gameplay
{
    [AddComponentMenu("Salazar/Actions/Add Inventory Item")]
    public class AddInventoryItemBehaviour : GenericBehaviour 
    {
        [SerializeField] InventoryItem item;

        public override void Behave ()
        {
            base.Behave ();

            MessageKit<InventoryItem>.post (GameMessage.AddInventoryItem, item);
        }
    }
}
