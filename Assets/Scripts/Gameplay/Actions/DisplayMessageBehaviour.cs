﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Salazar.Framework;
using Prime31.MessageKit;

namespace Salazar.Gameplay
{
    [AddComponentMenu("Salazar/Actions/Display Message")]
    public class DisplayMessageBehaviour : GenericBehaviour 
    {
        [SerializeField] string message;

        public override void Behave ()
        {
            base.Behave ();

            INNOVATHENS.UI.MessagePanelUI.ShowMessage (message);
        }
    }
}
