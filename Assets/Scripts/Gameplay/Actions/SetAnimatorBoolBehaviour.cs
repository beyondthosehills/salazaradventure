﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Salazar.Gameplay
{
    [AddComponentMenu("Salazar/Actions/Set Animator Bool Value")]
    public class SetAnimatorBoolBehaviour : GenericBehaviour 
    {
        [SerializeField] Animator animator;
        [SerializeField] string paramName;
        [SerializeField] bool value;

        public override void Behave ()
        {
            base.Behave ();

            animator.SetBool (paramName, value);
        }
    }
}
