﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Salazar.Framework;
using Prime31.MessageKit;

namespace Salazar.Gameplay
{
    [AddComponentMenu("Salazar/Actions/Remove Inventory Item")]
    public class RemoveInventoryItemBehaviour : GenericBehaviour 
    {
        [SerializeField] InventoryItem item;

        public override void Behave ()
        {
            base.Behave ();

            MessageKit<InventoryItem>.post (GameMessage.RemoveInventoryItem, item);
        }
    }
}
