﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Salazar.Framework;

namespace Salazar.Gameplay
{
    public class GenericBehaviour : MonoBehaviour 
    {
        

        protected bool behaveIsDone = false;

        public virtual void Behave ()
        {
            behaveIsDone = true;
        }

       
    }
}
