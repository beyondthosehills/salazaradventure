﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Salazar.NPC;

namespace Salazar.Gameplay
{
    public class PlayerAttack : MonoBehaviour 
    {
        [SerializeField] float initY;
        [SerializeField] float range = 1f;
        [SerializeField] float damageSpeed = 1f;
        [SerializeField] LineRenderer lineRenderer;

        bool attack = false;

    	// Update is called once per frame
    	void Update ()
        {
            CheckMouse (); 
            Attack ();
    	}

        RaycastHit hit;

        void Attack ()
        {
            if (!attack)
            {
                DrawRay (false);
                return;
            }

            DrawRay (true);

            if (!Physics.Raycast (this.transform.position, this.transform.forward, out hit, range))
                return;

            EnemyLife enemyLife = hit.transform.GetComponent<EnemyLife> ();

            if (enemyLife == null)
                return;

            enemyLife.OnDamage (damageSpeed);

        }

        void DrawRay (bool visible)
        {
            if (visible)
            {
                lineRenderer.SetPosition (0, this.transform.position + (Vector3.up * initY));
                lineRenderer.SetPosition (1, this.transform.position + (Vector3.up * initY) + (this.transform.forward * range));
                lineRenderer.enabled = true;
            }
            else
            {
                lineRenderer.enabled = false;
            }
        }

        void CheckMouse ()
        {
            if (Input.GetMouseButtonDown (0))
            {
                attack = true;
            }

            if (Input.GetMouseButtonUp (0))
            {
                attack = false;
            }
        }

      
    }
}
