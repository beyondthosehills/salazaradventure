﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.MessageKit;
using Salazar.Framework;


namespace Salazar.Gameplay
{
    public class InteractWithWorld : MonoBehaviour 
    {
        [SerializeField] float raycastRange = 1f;
        [SerializeField] LayerMask raycastMask;
    	
        InteractiveObject interactiveObject;

    	// Update is called once per frame
    	void Update () 
        {
            interactiveObject = Raycast ();

            MessageKit<bool>.post (GameMessage.CheckInteractiveObject, interactiveObject != null);


            if (Input.GetKeyUp (KeyCode.E))
            {
                if (interactiveObject != null)
                    interactiveObject.Interact ();
            }
    	}

        RaycastHit hit;

        InteractiveObject Raycast ()
        {
            
            if (Physics.Raycast (this.transform.position, this.transform.forward, out hit, raycastRange, raycastMask.value))
            {
                return hit.transform.GetComponent<InteractiveObject> ();
            }

            return null;
        }

        void OnDrawGizmos ()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawRay (this.transform.position, this.transform.forward * raycastRange);
        }
    }



}
