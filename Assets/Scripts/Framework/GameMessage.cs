﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Salazar.Framework
{

    public static class GameMessage
    {
        public static int CheckInteractiveObject = 5;   //<InteractiveObject>

        public static int AddInventoryItem = 10;    //<InventoryItem>
        public static int RemoveInventoryItem = 11; //<InventoryItem>

        public static int SetState = 20;    //<State>
        public static int UnsetState = 21;  //<State>
    }

}
