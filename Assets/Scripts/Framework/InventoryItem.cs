﻿using UnityEngine;

namespace Salazar.Framework
{
    [CreateAssetMenu(menuName="Salazar/Inventory Item")]
    public class InventoryItem : ScriptableObject
    {
        public string itemName;
        public Sprite image;
        public string description;
    }
}