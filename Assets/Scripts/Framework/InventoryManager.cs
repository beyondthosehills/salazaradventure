﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.MessageKit;

namespace Salazar.Framework
{
    public class InventoryManager : MonoBehaviour
    {
        private static InventoryManager instance;

        void Awake ()
        {
            if (instance != null)
                return;
            
            instance = this;

            MessageKit<InventoryItem>.addObserver (GameMessage.AddInventoryItem, AddItem);
            MessageKit<InventoryItem>.addObserver (GameMessage.RemoveInventoryItem, RemoveItem);

        }

        void OnDestroy ()
        {
            MessageKit<InventoryItem>.removeObserver (GameMessage.AddInventoryItem, AddItem);
            MessageKit<InventoryItem>.removeObserver (GameMessage.RemoveInventoryItem, RemoveItem);
        }

        public static bool HasItem (InventoryItem item)
        {
            if (instance == null)
                return false;

            if (instance.itemList.Contains (item))
                return true;

            return false;
        }


        List<InventoryItem> itemList = new List<InventoryItem>();


        void AddItem (InventoryItem item)
        {
            if (itemList.Contains (item))
                return;
            
            itemList.Add (item);

            Debug.LogFormat ("Add nventory item : {0}", item.name);
        }

        void RemoveItem (InventoryItem item)
        {
            if (!itemList.Contains (item))
                return;

            itemList.Remove (item);

            Debug.LogFormat ("Remove nventory item : {0}", item.name);
        }
    	
    }
}
