﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Prime31.MessageKit;

namespace Salazar.Framework
{

    public class StateManager : MonoBehaviour 
    {
        private static StateManager instance;

        void Awake ()
        {
            if (instance != null)
                return;

            instance = this;

            MessageKit<State>.addObserver (GameMessage.SetState, SetState);
            MessageKit<State>.addObserver (GameMessage.UnsetState, UnsetState);

        }

        void OnDestroy ()
        {
            MessageKit<State>.removeObserver (GameMessage.SetState, SetState);
            MessageKit<State>.removeObserver (GameMessage.UnsetState, UnsetState);
        }

        List<State> stateList = new List<State>();

        public static bool HasState (State state)
        {
            if (instance == null)
                return false;
                    
            if (instance.stateList.Contains (state))
                return true;

            return false;
        }

        void SetState (State state)
        {
            if (stateList.Contains (state))
                return;

            stateList.Add (state);

            Debug.LogFormat ("Set state : {0}", state.name);
        }

        void UnsetState (State state)
        {
            if (!stateList.Contains (state))
                return;

            stateList.Remove (state);

            Debug.LogFormat ("Unset state : {0}", state.name);
        }
    	
    }
}
